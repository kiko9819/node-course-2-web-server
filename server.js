const express = require("express");
const hbs = require("hbs");
const app = express();
const fs = require("fs");
const port = process.env.PORT || 3000;

hbs.registerPartials(__dirname + "/views/partials");
hbs.registerHelper("getCurrentYear",()=>{
  return new Date().getFullYear();
});
hbs.registerHelper("screamIt",(text)=>{
  return text.toUpperCase();
});
app.set("view engine","hbs");
app.use(express.static(__dirname + "/public"));

// other middleware
app.use((req,res,next)=>{
  var now = new Date().toString();
  var log = `${now} ${req.method} ${req.url}`;

  console.log(log);
  fs.appendFile("server.log",log+"\n",(err)=>{
    if(err){
      console.log("Unable to append server.log");
    }
  });
  next();
});
// // middleware for maintanance
// app.use((req,res,next)=>{
//   res.render("maintanance.hbs");
// });

app.get("/", (req, res) => {
  res.render("home.hbs",{
    pageTitle: "Home page",
    welcomeMessage: "Welcome to the homepage",
  })
});

app.get("/about", (req, res) => {
  res.render("about.hbs",{
    pageTitle:"About page",
  });
});

app.get("/projects",(req,res)=>{
  res.render("projects.hbs",{
    pageTitle: "This is the projects page",
    welcomeMessage: "Here I will be putting all of my projects",
  });
});

app.get("/bad", (req, res) => {
  res.send({
    error: "Unable to handle this shit"
  });
});

app.listen(port, function() {
  console.log(`Server is up on port ${port}`);
});
